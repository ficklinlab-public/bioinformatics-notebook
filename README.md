# The Bioninformatics Notebook

**Version**: v1.0

The Bioinformatics Notebook is a protocol for self-organization of a manually executed bioinformatics workflow. It was created and is updated by the Ficklin Computational and Bioinforamtics Dry Lab at Washington State University. 

Often, a bioinformatics task involves multiple software tools executed one after another.  Workflow software tools such as [Galaxy](https://galaxyproject.org/), [Snakemake](https://snakemake.readthedocs.io/en/stable/) or [Nextflow](https://www.nextflow.io/) are used to create automated workflows, and community-developed repositories such as [nf-core](https://nf-co.re/) provide as set of useful workflows for popular analyses.  In many cases, however, a bioninformatics workflow is experimental and the workflow is not fully mature. These types of manual workflows are full of trial-and-error.  Also, experimental bioinformatics can result in a disorganized file directory sturcture, making it difficult for others, who may come after, to make sense of what was done, creating unnecessarily and costly delays.  This protocol documents how to organize such a manual workflow to provide the following benefits:

- **Reproducibility**: once published online, the workflow, if properly follows this protocol, should be fully reproducible by anyone. This protocol makes use of a git repository to ensure the analyses meet reproducibilty standards of FAIR (findable, accessible, interoperable and reproducible) data.  
- **Shareability**: this protocol uses Git to make sharing of scripts within the workflow easy to share.  
- **Readability**: with everyone in the group following this protocol, finding results and understanding what was done by someone else is much easier to determine. 
- **Backup Ready**: critical data that needs backup is separated from intermediate data that can be recreated.  

This is a living document which is updated as changes or improvments are added.

## Guiding Principles
Design of the file structure for your project is up to you. Aside from the few guidelines listed below, it is up to you to make sure that your project is reproducible. The best way to ensure this is to consider, each time you add content, how you can provide enough information to ensure that your data and results can be found and are 100% reproducible.

## Creating Projects
Each project should have its own git repository. Follow these practices for creating repositories

- Give the project a short name that will uniquely identify what the project is for.
- Add a description when creating the project so that when others view the project they get a better sense for what the project is for.

## General Project Structure
### Project README.md files.

Each project should have a `README.md` file at the project root level that contains the following headers:
- Project Overview: provides a brief introduction that orients a new
- Project Contributors: Indicate the names of individuals who worked on the project and specify what their contributions are/will be.
- Directory Structure: describe each folder and file that resides in the current directory. Note: every directory should have a README.md file that contains at least this section.

### General Directory Structure
Every project represents a "workflow". In this case, the workflow is a manual execution of tasks. Additionally, even for projects that include a high degree of testing and exploration there should be components into which the project can be divided. Each of these components, or tasks, should have a separate directory.

To help others understand the order of the steps you performed, please give directories a two-digit numeric prefix so that they appear in the order they are processed. (e.g., 01-data_preparation, 02-network_construction, etc.). Otherwise, you can name directories anything you would like, however please follow these conventions:

- Name your directory a descriptive name for the files it contains.
- Give your directories "action" names (names that contain verbs describing what analysis was performed inside of it) or include the name of the tool if it's obvious what it does (e.g., GEMmaker, KINCv3, etc.).

Always create the following directories in the root-level of your project:

- 00-docs: meant to house all documentation about the project such as emails, meeting notes, publications, presentations, etc.
- 01-input_data: houses all input data, that is not reproducible. Also, simple pre-processing of input data (e.g., indexing of genome reference files) can go here too. Be sure to set all original data files as read-only so that they cannot be accidentally overwritten.
- 99-reports: houses files that were provided to collaborators for reporting progress. Each separate report should have a separate folder. The folder should have the date as the prefix to the directory in the form YYYY-MM-DD indicating the date the report was delivered to the collaborator.
- 99-pubs: houses files or links to files that will be used in publications. The README.md file should describe the workflow steps that were used to generate the results and the author should ensure that all those steps in the workflow are fully documented and reproducible.

### Input data (01-input_data) 
Place all input data that is not created with by the project inside of the 01-input_data folder. You can organize data in subfolders. Please follow theses rules when adding data:

- Do not rename original data files. If changes are needed then create a copy and document that you renamed it.
- Specify in the README a contact person who can answer questions about the data (if appropriate).
- An index file for mapping data to samples
- Information about columns in spreadsheets or tabular data.

## Task Directories
### Directory README.md Files
Every directory should contain a README.md file. Instructions for formatting README files can be found here: https://docs.gitlab.com/ee/user/markdown.html

Every README.md (other than the root README.md) should provide at a minimum these two sections:

- Directory Overview: Provide a brief description for the purpose of the directory.
- Directory Structure: Provides a brief description of each file and subdirectory in the directory.

If any analysis was performed within the directory all the command-executed should be in a heading titled **Methods** or in a separate `Methods.md` file. The purpose of this section is to provide step-by-step instructions for someone to reproduce any results files in the directory.

### Describing Methods
All results should be reproducible. This requires that we provide step-by-step instructions for how all files in the project are generated and analyzed. There are multiple approaches you can follow to ensure reproducibility.

#### Methods.md
If you prefer to use markdown to describe your methods, please name the file `Methods.md` rather than use the **Methods** header in the README.md file.

#### Jupyter or R Notebooks
If you perform analysis using R or Python you may place your step-by-step instructions in a Jupyter notebook rather than directly in the README.md file. If you use a Jupyter notebook, be sure to describe it in the `README.md` file and indicate when they should be used in order to reproduce results. You could give your notebooks numeric prefixes to help indicate order they should be executed. 

Here are some guidelines for developing a readable notebook
- MOST IMPORTANT: always keep in mind the notebook isn't just for you, it's also for those who come after you who might want to use your work or learn from what you've done. Be verbose and make sure you keep this reader in mind as you work in the notebook. 
  - If you are confused by a tool or parameters then odds are someone else will be to. Document your confusion and the answers you found.
  - Always state why you are doing a particular task or function call
  - Remember to write down what you learned or decisions you made from the work that was done in a notebook.
  -  Document pitfalls and failures as well.
- Be sure that the sequence of cells follows a thought process (i.e. don't jump around in the notebook).
- Avoid reusing the same variable name to store refactored data frames. Because if someone re-runs a cell they may get different results.

#### R Studio Files or Python Scripts

If you prefer to work directly with R Studio or Python rather than Jupyter Notebooks for data analysis please be sure to intersperse your code with comments to help others fully understand what each block of code is doing. If you use R Studio or Python scripts be sure to describe them in the README.md file and indicate when they should be used in order to reproduce results.

## File and Directory Suffixes
Please add the following suffixes to working directories accordingly:

- **-DEPRECATED**: Use this for directories where the analysis was completed but is no longer being used. This allows you to keep analysis scripts for later reference (i.e., not delete them) but indicate to others these folders should be ignored.  The files within should follow all the protocols described in this document and be fully reproducible. But files in such directories should not be used for other downstream analyses unless those directories are also marked as deprecated.
- **-EXPERIMENTAL**: Use this for analyses that are temporary or exploratory. Often we just want to explore what a tool can do or experiment with analyses on data. Adding this suffix alerts others that thay can ignore this directory. Files in such directories should not be used for other downstream analyses.
- **-FROZEN**: Use this for an analysis that has been used in a publication. A directory that is frozen should have no other edits, changes, or updates to ensure reproducibility.

## Dealing with Large Files
We cannot commit large files to Git repositories. However sometimes a reader needs to find large files to reproduce your work. In this case, always include comments in your README.md or other documents indicating where a reader can find data.

### Committing
#### What Files to Commit
Not all files should be committed to your repository. Network bandwidth and space on GitHub or Gitlab is limited. 

Please follow these guidelines when committing files:
- **README.md** files: always add a README.md to every directory you create indicating its purpose and what you are doing inside of it. If you have subdirectories, your README should describe the purpose of each and what a reader will find in each one.
- *Result files*: commit results from analyses only if they obey these rules: 
  - The file is small (i.e. no larger than a few megabytes at least)
  - The file is small and considered "final" for an analysis and could be used in publication.
  - The file is small and used by other downstream analyses.
- *Images*: commit images if you can refer to them in the README.md file.  Committing too many images can cause slowness with the git repository.
- *Jupyter Notebooks*: always commit jupyter notebooks. If the notebook exceeds a few megabytes then clear the notebook of results before committing.
- *Scripts*: always commit scripts in R, Python, PHP, etc.
- *HPC scripts*: always commit your SLURM (HPC cluster) or HTCondor (Open Science Grid (OSG)) scripts for launching jobs.

#### When to Commit
Commit and push to Git often! Best practice is to commit what you worked on anytime you leave your computer for a significant amount of time. This will prevent loss and ensure that others have up-to-date information.

Remember that git supports branching, so if you want to explore a tangent without updating documentation, create a branch and commit that. Later if you are happy with your "branched" work you can merge it into the master branch.

### Exceptions
When using software packages that are also managed via some git repository it can be problematic to add files to your Git repository. For example, GEMmaker is often cloned from GitHub. When it is cloned it has its own Git repository that may conflict with your project Git repository. In this case if you have GEMmaker as part of your workflow and in your directory structure you will want to include the `nextflow.config`, the SLURM batch script, and any report files once completed. But if you try to add those files while in the GEMmaker directory you will add those to the GEMmaker git project rather than your GitLab project. Also, GEMmaker has its own `README.md`` so you cannot add a new one. In cases such as GEMmaker, you should add config, submit and report files to your Git project from one directory above (not inside GEMmaker) and you do not need to add a README.md file.

## Quality Control
To maintain high quality in our work, and to support full reproducibility, timely reviews should be performed by another lab member. A checklist has been created to aid in this review.

## Backups
The `00-docs`, `01-Input_data`, and any `99-` directories should be backed up automatically. All other directories used for analysis should be created starting with a 02 prefix and if the READMEs are complete and all scripts are committed to the repository all intermdiate results should be reproducible.  

## Containerized Software
To enhance reproducibilty, you can use dockerized software for every step of the workflow, This will allow the Git repository to be published for public viewing and make it easy for others to reproduce your work.

